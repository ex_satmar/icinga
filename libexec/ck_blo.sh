#!/bin/bash
speed1=`snmpget -v1 -c $2 $4 $6 | cut -d'"' -f2`
status1=`snmpget -v1 -c $2 $4 $8 | cut -d:  -f4 |cut -d' ' -f2`
speed2=`snmpget -v1 -c $2 $4 ${10} | cut -d'"' -f2`
status2=`snmpget -v1 -c $2 $4 ${12} | cut -d:  -f4 |cut -d' ' -f2`
spd1=`snmpget -v1 -c $2 $4 $6 | cut -d% -f1 | cut -d'"' -f2`
spd2=`snmpget -v1 -c $2 $4 ${10} | cut -d% -f1 | cut -d'"' -f2`

if [ $status1 -ne 1 ] && [ $status2 -ne 1 ]
then
result="CRITICAL! Both Blower1 and Blower2 are Down"
elif [ $status1 -ne 1 ] && [ $status2 -eq 1 ]
then
result="WARNING! Blower1 is Down,BLower2 is Up speed:$speed2|Blower2=$spd2%;80;90;0;100"
elif [ $status2 -ne 1 ] && [ $status1 -eq 1 ]
then
result="WARNING! Blower2 is Down, Blower1 is Up speed:$speed1|Blower1=$spd1%;80;90;0;100"
elif [ $status1 -eq 1 ] && [ $status2 -eq 1 ]
then
result="OK! Blower1 speed:$speed1, and Blower 2 speed:$speed2|Blower1=$spd1%;80;90;0;100; Blower2=$spd2%;80;90;0;100"
else
result="CRITICAL! Status Unknown"
fi

if [ -n "`echo $result | grep CRITICAL`" ] ; then
      echo $result
      exit 2
    fi
    if [ -n "`echo $result | grep WARNING`" ] ; then
      echo $result
      exit 1
    fi
   echo $result
  exit 0



