#!/bin/bash
connections=`netstat -pant | grep :3868 |grep ESTABLISHED |grep $1 | grep $2 |wc -l`

if [ $connections -gt 0 ]; then
  echo -n "There are $connections established"
  exit 0
else
  echo -n "There are no established connections"
  exit 2
fi


