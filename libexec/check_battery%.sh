#!/bin/bash
bat=`snmpget -v1 -c $2 $4 $6 |  cut -d':' -f4 | cut -d' ' -f2`

if [ ${8} -lt ${10} ] ; then
        echo "UNKNOWN - Warning level is less then Crit"
exit 2
fi

if [ $bat -lt ${10} ]
then
echo "CRITICAL! Battery% of UPS :$bat%|Battery%=$bat%;${10};${8};0;100"
exit 2
elif [ $bat -lt $8 ]
then
echo "WARNING! Battery% of UPS :$bat%|Battery%=$bat%;${10};${8};0;100"
exit 1
else
echo "OK! Battery% of UPS :$bat%|Battery%=$bat%;${10};${8};0;100"
exit 0
fi




