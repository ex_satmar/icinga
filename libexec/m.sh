#!/bin/bash
export ORACLE_HOME=/u01/app/oracle/product/11.2.0/client_1
export PATH=$PATH:/u01/app/oracle/product/11.2.0/client_1/bin
export LD_LIBRARY_PATH=/u01/app/oracle/product/11.2.0/client_1/lib

PROGNAME=`basename $0`
PROGPATH=`echo $0 | sed -e 's,[\\/][^\\/][^\\/]*$,,'`
REVISION="2.1.1"

. $PROGPATH/utils.sh


d=`date`
echo "========="$d
echo "Processing.................."
ccap_cnt=`sqlplus -s knowitro/knowitro@cust1<<EOF
set pagesize 0 feedback off verify off heading off echo off
SELECT count(*)
  FROM file_status@blus1 fs, usage_files_to_process@blus1 fp
 WHERE fs.file_id = fp.file_id
   AND TRIM (file_name) LIKE '%cdr'
   AND file_type = 0
   AND ext_contact_id = 21
   AND entry_create_dt >= TO_DATE ('2015-02-24', 'YYYY-MM-DD');

EOF`
echo "CCAP Pending files Count is :"$ccap_cnt

sqlplus -s knowitro/knowitro@cust1<<EOF
column c1 heading "FILE NAME" format a27

SELECT * FROM (select trim(substr(file_name,4)) c1,to_char(entry_create_dt,'dd/mm/yyyy hh24:mi:ss') "DATE",
total_records "TOTAL" ,num_good "GOOD REC" ,num_soft_errors "SOFTERR"
 from file_status@blus1 where entry_create_dt > TO_DATE ('2015-02-23', 'YYYY-MM-DD') 
 and file_name like '%AT12015%' order by file_id desc)
 WHERE ROWNUM<4;
EOF
urp_cnt=`sqlplus -s knowitro/knowitro@main1<<EOF1
set pagesize 0 feedback off verify off heading off echo off
SELECT count(*)
FROM usage_record_main urm, extract_audit_control eac
WHERE urm.extract_file_id = eac.extract_file_id AND extract_status <>7 ;

EOF1`
echo "Extraction pending records  count is :"$urp_cnt

echo "Extractedd file details"

sqlplus -s knowitro/knowitro@main1<<eof3

select * from (SELECT extract_file_id, to_char(extraction_start_time,'dd/mm/yyyy hh24:mi:ss') extraction_start_time,
to_char(extraction_end_time,'dd/mm/yyyy hh24:mi:ss') extraction_stop_time, c.RECORDS_COUNT
  FROM extract_audit_control c
 WHERE process_name = 'LTP'
 order by extract_file_id desc) where rownum<3;

eof3

