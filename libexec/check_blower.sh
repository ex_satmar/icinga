#!/bin/bash
speed1=`snmpget -v1 -c $1 $2 $3 | cut -d'"' -f2`
status1=`snmpget -v1 -c $1 $2 $4 | cut -d:  -f4 |cut -d' ' -f2`
speed2=`snmpget -v1 -c $1 $2 $5 | cut -d'"' -f2`
status2=`snmpget -v1 -c $1 $2 $6 | cut -d:  -f4 |cut -d' ' -f2`
spd1=`snmpget -v1 -c $1 $2 $3 | cut -d% -f1 | cut -d'"' -f2`
spd2=`snmpget -v1 -c $1 $2 $5 | cut -d% -f1 | cut -d'"' -f2`
if [ ${7} -gt ${8} ] ; then
        echo "UNKNOWN - Warning level is more then Crit"
exit 2
fi
if [ $status1 -ne 1 ] && [ $status2 -ne 1 ]
then
result="CRITICAL! Both Blower1 and Blower2 are Down"
elif [ $status1 -ne 1 ] && [ $status2 -eq 1 ]
then
result="WARNING! Blower1 is Down,BLower2 is Up speed:$speed2|Blower2=$spd2%;${7};${8};0;100"
elif [ $status2 -ne 1 ] && [ $status1 -eq 1 ]
then
result="WARNING! Blower2 is Down, Blower1 is Up speed:$speed1|Blower1=$spd1%;${7};${8};0;100"
elif [ $status1 -eq 1 ] && [ $status2 -eq 1 ]
then
result="OK! Blower1 speed:$speed1, and Blower 2 speed:$speed2|Blower1=$spd1%;${7};${8};0;100; Blower2=$spd2%;${7};${8};0;100"
else
result="CRITICAL! Status Unknown"
fi

if [ -n "`echo $result | grep CRITICAL`" ] ; then
      echo $result
      exit 2
    fi
    if [ -n "`echo $result | grep WARNING`" ] ; then
      echo $result
      exit 1
    fi
   echo $result
  exit 0



