#!/bin/sh
export ORACLE_HOME=/u01/app/oracle/product/11.2.0/client_1
export PATH=$PATH:/u01/app/oracle/product/11.2.0/client_1/bin
export LD_LIBRARY_PATH=/u01/app/oracle/product/11.2.0/client_1/lib

PROGNAME=`basename $0`
PROGPATH=`echo $0 | sed -e 's,[\\/][^\\/][^\\/]*$,,'`
REVISION="2.1.1"

. $PROGPATH/utils.sh


print_usage() {
echo "  $PROGNAME -a <ORACLE_SID> -b <USER> -c <PASS>  -e <CRITICAL> -f <WARNING>"
}
if [ ${8} -lt ${10} ] ; then
        echo "UNKNOWN - Warning level is more then Crit"
        exit $STATE_UNKNOWN
    fi
 result=`sqlplus -s ${4}/${6}@${2} << EOF
set pagesize 0
set numf '9999999.99'
set linesize 120
set serveroutput on
set feedback off
declare
cursor c1 is 
select tablespace_name from user_tablespaces
where tablespace_name not in ('TEMP');
tname varchar2(100);
total number;
free number;
per number;
begin
for i in c1 loop
select a.tablespace_name,a.total,b.free,100-round(b.free/a.total,2)*100 into tname,total,free,per from (select tablespace_name,sum(bytes)/1024/1024 total from 
dba_data_files
group by tablespace_name) a, (select tablespace_name,sum(bytes)/1024/1024 free from 
dba_free_space
group by tablespace_name)  b
where  a.tablespace_name=b.tablespace_name
and a.tablespace_name =i.tablespace_name;

if per >  80 then
dbms_output.put_line('CRITICAL!Total space:'||total||' free:'||free||' Used Percentage:'||per||'% for Tablespace:'||tname);
DBMS_OUTPUT.PUT_LINE('--------------------------------------------------------');
elsif per > 70 then
dbms_output.put_line('WARNING!Total space:'||total||' free:'||free||' Used Percentage:'||per||'% for Tablespace:'||tname);
DBMS_OUTPUT.PUT_LINE('--------------------------------------------------------');
end if;

end loop;
end;
/

EOF`
    if [ -n "`echo $result | grep CRITICAL`" ] ; then
      echo $result
      exit $STATE_CRITICAL
    fi
    if [ -n "`echo $result | grep WARNING`" ] ; then
      echo $result
      exit $STATE_WARNING
    fi
   echo $result
  exit $STATE_OK

