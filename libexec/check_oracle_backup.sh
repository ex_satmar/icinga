#!/bin/bash
export ORACLE_HOME=/u01/app/oracle/product/11.2.0/client_1
export PATH=$PATH:/u01/app/oracle/product/11.2.0/client_1/bin
export LD_LIBRARY_PATH=/u01/app/oracle/product/11.2.0/client_1/lib

sqlplus -s $2/$4@$6 << EOF
set pagesize 0
set numf '9999999.99'
select SESSION_KEY, INPUT_TYPE, STATUS,
to_char(START_TIME,'mm/dd/yy hh24:mi') start_time,
to_char(END_TIME,'mm/dd/yy hh24:mi') end_time,
elapsed_seconds/3600 hrs
from V$RMAN_BACKUP_JOB_DETAILS
order by session_key;
EOF
