#!/bin/bash

n=`df | wc -l`
c=`expr $n - 1`
a=`df | awk '{print $4}' | tail -n$c`
b=`df | awk '{print $3}' | tail -n$c`

avail=0
for i in ${a[@]};
do
avail=`expr $avail + $i`
done

used=0
for i in ${b[@]};
do
used=`expr $used + $i`
done

tot=`expr $used + $avail`
per=`echo $(( ($used * 100) / $tot))`
Gused=`echo $(( $used / 1048576 ))`
Gavail=`echo $(( $avail / 1048576 ))`
Gtot=`echo $(( $tot / 1048576 ))`
warn=`echo $(( ( $Gtot * $1 ) / 100 ))`
crit=`echo $(( ( $Gtot * $2 ) / 100 ))`

if [ "$warn" -gt "$crit" -o "$warn" -eq "$crit" ]; then
   echo "Unknown: [crit] must be larger than [warn]"
        exit 3
fi

if [ "$Gused" -lt "$warn" -o "$Gused" -eq "$warn" ]; then
        echo "OK. TOTAL Used Space:$Gused"G", $per%"
        exit 0
 elif [ "$Gused" -gt "$warn" -a "$Gused" -lt "$crit" ]; then
        echo "Warning. TOTAL Used Space:$Gused"G", $per%"
        exit 1
 elif [ "$Gused" -gt "$crit" ]; then
        echo "Critical. TOTAL Used Space:$Gused"G", $per%"
        exit 2
 else
   echo "Unknown"
   exit 3
fi







